const express = require('express')
const app = express()
const fetch = require("node-fetch")
const parkings = require('./parkings.json')

app.use(express.json())

app.get('/', (req, res) => {
    res.json("HELLO WORLD EXAPRINT TESTS")
}) 
app.get('/parkings/:id', (req, res) => {
    const id = parseInt(req.params.id)
    const parking = parkings.find(parking => parking.id === id)
    res.status(200).json(parking)
})

app.get('/parkings/new', (req, res) => {
    res.json("Ma nouvelle feature !")
})

app.get('/parkings', (req, res) => {
    parkings.push(req.body)
    res.status(200).json(parkings)
});




const port = process.env.PORT || 8080 ;

app.listen(port, () => {
    console.log("server sur écoute")
})

