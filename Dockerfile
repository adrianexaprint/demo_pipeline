FROM node:10

WORKDIR /usr/src/app

COPY . .

RUN npm install

##EXPOSE 8080

ENV NODE_ENV=production

CMD [ "node", "index.js" ]